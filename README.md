This project started as a .css file, that I saved some html color codes in. I named it .css, so I could see color previews in my text editor.
I then decided to make it a .sass file, so I could use it to import my custom named colors, into my existing sass projects.
I've also included a couple of example .sass colorschemes in this repo, that you should be able to just download into your stylesheet directory in project that supports sass, and do an `@import "pinkPurpleScheme"` or whatever other sass file you want.

I have plans to add more colors and color schemes to this repo. I also want to organize the colors in the main colors files a bit better, but at the moment am having a problem with my text editor. I use vim, and am using this plugin, https://github.com/gko/vim-coloresque. It fails to show previews for certain colors, and shows incorrect previews for certain colors. It's also an unmainted plugin, so I'll have to spend some time finding a good fork, or alternative plugin.

I've also linked some resources, I used to find colors and schemes, in the resources folder.
